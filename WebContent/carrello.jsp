<%@page import="com.eComm.Gestione.GestisciProdotti"%>
<%@page import="com.eComm.Connessione.ConnettoreDB"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.eComm.classi.Prodotto"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Carrello</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Codice</th>
							<th>Prezzo (&euro;)</th>
							<th>Quantita</th>
							<th>Azioni</th>
						</tr>
					</thead>
					<tbody>

						<%
						HttpSession sessione=request.getSession();
						ArrayList<Prodotto> elenco = (ArrayList<Prodotto>) sessione.getAttribute("carrello");
						
						
						if(elenco==null){
							response.sendRedirect("carrelloVuoto.html");
							return;
						}
					   /*  GestisciProdotti gestore = new GestisciProdotti();
						elenco = gestore.findAllProd();  */

						String riga_risultante = "";
 
						for (int i = 0; i < elenco.size(); i++) {
							Prodotto temp = elenco.get(i);

							riga_risultante += "<tr>";
							riga_risultante += "<td>" + temp.getNome() + "</td>";
							riga_risultante += "<td>" + temp.getCodice() + "</td>";
							riga_risultante += "<td>" + temp.getPrezzo() + "</td>";
							riga_risultante += "<td>" + temp.getQuantita() + "</td>";
							riga_risultante += "<td><a href='rimuoviDaCarrello.jsp?cod=" + temp.getCodice() + "'>Rimuovi</a></td>";
							riga_risultante += "</tr>";
						}

						out.print(riga_risultante);
						%>

					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
		<a href="elencoProdottiCarr.jsp" class="btn btn-primary" role="button">Inserisci Altro</a>
		</div>
		<div class="row">
		<a href="index.html" class="btn btn-success" role="button">Home</a>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>
</html>