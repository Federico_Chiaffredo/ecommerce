<%@page import="com.eComm.Gestione.GestisciProdotti"%>
<%@page import="com.eComm.classi.Prodotto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.eComm.Connessione.ConnettoreDB"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Rimuovi dal Carrello</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">

			
			<!-- 	int var_id = Integer.parseInt(request.getParameter("idProdotto"));
				
				sessione.setAttribute("id", var_id); -->
				
				<% HttpSession sessione=request.getSession();
				String codice=(String) request.getParameter("cod");
				GestisciProdotti gestore = new GestisciProdotti();
				Prodotto prod=new Prodotto();
				
				ArrayList<Prodotto> prodotti= (ArrayList<Prodotto> ) sessione.getAttribute("carrello");
				for(Prodotto c:prodotti){
					if(c.getCodice().equals(codice)){
						prod=c;
					}
				}
				Prodotto prodSel = gestore.findById(gestore.ricercaIdByCod(codice));
				%>  
				
				

				<form action="rimuoviCarrello" method="POST">
					<h1>Rimozione del Prodotto dal carrello</h1>
					<%-- <p><%out.print(prodSel.toString()); %></p> --%>
					<div class="form-group">
						Nome:
					<input type="text" class="form-control" id="pwd" name="nome" value="<%out.print(prod.getNome()); %>" readonly>
					</div>
					<div class="form-group">
						Codice:
						<input type="text" class="form-control"  name="codice" value="<%out.print(prod.getCodice()); %>" readonly>
					</div>
					<div class="form-group">
						Prezzo (&euro;):
						<input type="text" class="form-control" id="pwd" name="prezzo" value="<%out.print(prod.getPrezzo()); %>" readonly>
					</div>

					<div class="form-group">
						Quantita:
						<input type="text" class="form-control" id="pwd" name="quantita" value="<%out.print(prod.getQuantita()); %>">
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-danger btn-block">Elimina</button>
					</div>
				</form>
				
				<form action="carrello.jsp" method="POST">
					<button type="submit" class="btn btn-primary btn-block">Indietro</button>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>

		<%-- <div class="row">
			<div class="col">
				<%
					/* String nome = "Giovanni";
				
					out.print(nome); */
					
					/* HttpSession sessione = request.getSession();
			
					out.print(sessione.getAttribute("stud_id"));
					out.print(sessione.getAttribute("stud_nome"));
					out.print(sessione.getAttribute("stud_cognome"));
					out.print(sessione.getAttribute("stud_matricola"));
					out.print(sessione.getAttribute("stud_sesso")); */
					
					
				%>
				
				
			</div>
		</div>	 --%>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>
</html>