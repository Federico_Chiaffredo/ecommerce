package com.eComm.Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eComm.Gestione.GestisciProdotti;
import com.eComm.classi.Utenti;

public class LoginServlet extends HttpServlet{
	
	private GestisciProdotti ges=new GestisciProdotti();
	ArrayList<Utenti>elencoUtenti=ges.findAllUtenti();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String var_username = request.getParameter("username");
		String var_password = request.getParameter("password");
		
		HttpSession sessione = request.getSession();
		sessione.setAttribute("isAuth", false);
		sessione.setAttribute("admin", false);
		
		if(sessione.getAttribute("isAuth") != null &&  (boolean) sessione.getAttribute("admin")) {
			response.sendRedirect("elencoProdottiMod.jsp");
			return;
		}
//		if(sessione.getAttribute("isAuth") != null &&  !(boolean) sessione.getAttribute("admin")) {
//			response.sendRedirect("elencoProdottiCarr.jsp");
//			return;
//		}
		for(Utenti u:elencoUtenti) {
		if(var_username.equals(u.getNomeUtente()) && var_password.equals(u.getPasswordUtente())&& u.getRuolo().equals("amm")) {
			sessione.setAttribute("isAuth", true);
			sessione.setAttribute("admin", true);
			sessione.setAttribute("user", var_username);
			sessione.setAttribute("passw", var_password);
			
			response.sendRedirect("elencoProdottiMod.jsp");
			return;
		}
		else if(var_username.equals(u.getNomeUtente()) && var_password.equals(u.getPasswordUtente())&&u.getRuolo().equals("base")) {
			sessione.setAttribute("isAuth", true);
			sessione.setAttribute("admin", false);
			sessione.setAttribute("user", var_username);
			sessione.setAttribute("passw", var_password);
			
			response.sendRedirect("elencoProdottiCarr.jsp");
			return;
		}
		}
		if(! (boolean) sessione.getAttribute("isAuth")) {
			response.sendRedirect("errorelogin.html");
		}
		
	}
	
	
}
