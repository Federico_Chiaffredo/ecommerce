package com.eComm.Servlets;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eComm.Connessione.ConnettoreDB;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class UpdateServlet extends HttpServlet{

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
		HttpSession sessione=request.getSession();
		int a=(int) sessione.getAttribute("id");
		String varNome = request.getParameter("nome");
		String varCodice = request.getParameter("codice");
		String varPrezzo=request.getParameter("prezzo");
		int varQuantita=Integer.parseInt(request.getParameter("quantita"));
		
		
		try {
			Connection con=(Connection) ConnettoreDB.getIstanza().getConnessione();
			String queryUpdate="UPDATE prodotti SET nome=?,codice=?,prezzo=?,quantita=? WHERE id=?";
			PreparedStatement ps=(PreparedStatement) con.prepareStatement(queryUpdate);
			ps.setString(1, varNome);
			ps.setString(2, varCodice);
			ps.setString(3, varPrezzo);
			ps.setInt(4, varQuantita);
			ps.setInt(5, a);
			int ris=ps.executeUpdate();
			
			if(ris>0) {

				response.sendRedirect("elencoProdottiMod.jsp");
			}
			else {

				response.sendRedirect("OOPS.html");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
	
    /**
     * Funzione che ritorna l'id di un prodotto dato il suo codice
     * @param varCod
     * @return -1 se non trovato || numero id se trovato
     */
    private int ricercaIdByCod(String varCod) {
        int id = -1;
        Connection con;
        try {
            con = (Connection) ConnettoreDB.getIstanza().getConnessione();
            String querySelect = "SELECT id FROM prodotti where codice= ?";
            PreparedStatement ps=(PreparedStatement) con.prepareStatement(querySelect);

            ps.setString(1, varCod);
            ResultSet risultato= ps.executeQuery();

            if(risultato.next())
                id=risultato.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

	
	
	
	
	
	
	
	
	
	
	
	
}
