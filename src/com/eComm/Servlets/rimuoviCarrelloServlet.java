package com.eComm.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eComm.classi.Prodotto;

public class rimuoviCarrelloServlet extends HttpServlet{
	
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		HttpSession sessione=request.getSession();
		String nome=request.getParameter("nome");
		String codice=request.getParameter("codice");
		String prezzo=request.getParameter("prezzo");
		int quantita=Integer.parseInt(request.getParameter("quantita"));
		
		ArrayList<Prodotto>elenco=(ArrayList<Prodotto>) sessione.getAttribute("carrello");
		
		for(int i=0;i<elenco.size();i++) {
			if(elenco.get(i).getCodice().equals(codice)) {
				if(quantita<elenco.get(i).getQuantita()) {
					quantita=elenco.get(i).getQuantita()-quantita;
					elenco.remove(i);
					Prodotto temp=new Prodotto(nome,codice,prezzo,quantita);
					elenco.add(temp);
					sessione.removeAttribute("carrello");
					sessione.setAttribute("carrello", elenco);
					response.sendRedirect("carrello.jsp");
					return;
					
				}
				if(quantita>=elenco.get(i).getQuantita()) {
					elenco.remove(i);
					sessione.removeAttribute("carrello");
					sessione.setAttribute("carrello", elenco);
					response.sendRedirect("carrello.jsp");
					return;
					
				}
			}
		}
		
		
	}
}
