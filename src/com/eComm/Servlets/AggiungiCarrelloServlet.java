package com.eComm.Servlets;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eComm.classi.Prodotto;

public class AggiungiCarrelloServlet extends HttpServlet{

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ArrayList<Prodotto> elencoC=null;
		HttpSession sessione=request.getSession();
		
		if(sessione.getAttribute("carrello")==null) {
			elencoC=new ArrayList<Prodotto>();
		}
		else {
			elencoC=(ArrayList<Prodotto>) sessione.getAttribute("carrello");
		}
		
		String varNome=request.getParameter("nome");
		String varCodice=request.getParameter("codice");
		String varPrezzo=request.getParameter("prezzo");
		String varQuantita=request.getParameter("quantita");
		
		Prodotto prTemp=new Prodotto(varNome,varCodice,varPrezzo,Integer.parseInt(varQuantita));
		
		elencoC.add(prTemp);
		sessione.setAttribute("carrello", elencoC);
		
		response.sendRedirect("elencoProdottiCarr.jsp");
	}
}
