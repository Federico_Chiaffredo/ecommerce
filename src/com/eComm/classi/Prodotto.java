package com.eComm.classi;

public class Prodotto {
	int id;
	

	private String nome;
	private String codice;
	private String prezzo;
	private int quantita;
	
	
	public Prodotto(){
		
	}
	
	public Prodotto(String varNom,String varCod,String varPr,int varQta){
		this.nome=varNom;
		this.codice=varCod;
		this.prezzo=varPr;
		this.quantita=varQta;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	
	
	@Override
	public String toString() {
		return "Prodotto [nome=" + nome + ", codice=" + codice + ", prezzo=" + prezzo + ", quantita=" + quantita + "]";
	}
	
}
