package com.eComm.classi;

public class Utenti {
	int id;
	String nomeUtente;
	String passwordUtente;
	String ruolo;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomeUtente() {
		return nomeUtente;
	}
	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}
	public String getPasswordUtente() {
		return passwordUtente;
	}
	public void setPasswordUtente(String passwordUtente) {
		this.passwordUtente = passwordUtente;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Utenti [id=" + id + ", nomeUtente=" + nomeUtente + ", passwordUtente=" + passwordUtente + ", ruolo="
				+ ruolo + "]";
	}
}
