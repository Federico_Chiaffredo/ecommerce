package com.eComm.Gestione;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.eComm.Connessione.ConnettoreDB;
import com.eComm.classi.Prodotto;
import com.eComm.classi.Utenti;
import com.mysql.jdbc.PreparedStatement;

public class GestisciProdotti {

	
	
public ArrayList<Prodotto> findAllProd() throws SQLException{
		
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, codice, prezzo, quantita FROM prodotti";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto prodTemp = new Prodotto();
			prodTemp.setId(risultato.getInt(1));
			prodTemp.setNome(risultato.getString(2));
			prodTemp.setCodice(risultato.getString(3));
			prodTemp.setPrezzo(risultato.getString(4));
			prodTemp.setQuantita(risultato.getInt(5));
			
			elenco.add(prodTemp);
		}
		
		return elenco;		
	}




public ArrayList<Utenti> findAllUtenti() {
	
	ArrayList<Utenti> elencoUtenti = new ArrayList<Utenti>();
	Connection conn;
	try {
		conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT id, nomeUtente,passwordUtente, ruolo FROM utenti";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Utenti uTemp = new Utenti();
			uTemp.setId(risultato.getInt(1));
			uTemp.setNomeUtente(risultato.getString(2));
			uTemp.setPasswordUtente(risultato.getString(3));
			uTemp.setRuolo(risultato.getString(4));
			
			elencoUtenti.add(uTemp);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

	
	return elencoUtenti;		
}





public Prodotto findById(int var_id)  throws SQLException{

	Connection conn = ConnettoreDB.getIstanza().getConnessione();

	String query = "SELECT id, nome, codice, prezzo, quantita FROM prodotti WHERE id = ?";
	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	ps.setInt(1, var_id);
	
	ResultSet risultato = ps.executeQuery();
	risultato.next();
	
	Prodotto prodTemp = new Prodotto();
	prodTemp.setId(risultato.getInt(1));
	prodTemp.setNome(risultato.getString(2));
	prodTemp.setCodice(risultato.getString(3));
	prodTemp.setPrezzo(risultato.getString(4));
	prodTemp.setQuantita(risultato.getInt(5));
	
	return prodTemp;
	
}



public int ricercaIdByCod(String varCod) {
    int id = -1;
    Connection con;
    try {
        con = (Connection) ConnettoreDB.getIstanza().getConnessione();
        String querySelect = "SELECT id FROM prodotti WHERE codice = ?";
        PreparedStatement ps=(PreparedStatement) con.prepareStatement(querySelect);

        ps.setString(1, varCod);
        ResultSet risultato= ps.executeQuery();

        if(risultato.next())
            id=risultato.getInt(1);

    } catch (SQLException e) {
        e.printStackTrace();
    }
    return id;
}




}
